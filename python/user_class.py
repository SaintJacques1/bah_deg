import random
import math

from random import randint

# Generate name and last_name of User
def get_personal_data():
    vowels_lower_rus = "аоиэуы"
    vowels_upper_rus = vowels_lower_rus.upper()
    constants_lower_rus = "бвгджзйклмнпрстфхцчщщ"
    constants_upper_rus = constants_lower_rus.upper()

    name = random.choice(vowels_upper_rus)

    for i in range(random.randint(1, 3)):
        name += random.choice(constants_lower_rus)
        name += random.choice(vowels_lower_rus)


    last_name = random.choice(vowels_upper_rus)
    last_name = random.choice(constants_upper_rus)

    for i in range(random.randint(3, 6)):
        last_name += random.choice(vowels_lower_rus)
        last_name += random.choice(constants_lower_rus)

    return name, last_name

# Generate email of User
def get_email():
    mails = ["@gmail.com", "@yandex.com", "@mail.ru"]

    constants_lower_eng = "bcdfghjklmnpqrstvxz"
    vowels_lower_eng = "aeiou"
    email = random.choice(constants_lower_eng)



    for i in range(random.randint(1, 7)):
        email += random.choice(vowels_lower_eng)
        email += random.choice(constants_lower_eng)

    return email + random.choice(mails)

# Generate phone number of User
def get_number():
    range_start = 10**(10-1)
    range_end = (10**10)-1
    number = "+7"+str(randint(range_start, range_end))

    return number

# Generate sex of User
def get_sex():
    gender_values = ("Мужчина", "Женщина")
    sex = random.choice(gender_values)

    return sex

# Generate random bool value
def get_bool():
    return random.choice([False, True])

# Return random choice of cities
def get_random_city():
    city_from_file = open("cities.txt").read().splitlines()
    cites_list = list(city_from_file)

    return random.choice(cites_list)

# Get age of User
def get_age():
    return random.randint(18, 59)

# Return User password
def get_password():
    range_start = 10 ** (10 - 1)
    range_end = (10 ** 10) - 1

    return randint(range_start, range_end)

# Сумма покупок
def get_amount_of_purchases():
    return random.randint(5, 50)

def get_number_of_purchase():
    return random.randint(3000, 300000)

def create_user():
    (name, last_name) = get_personal_data()
    age = get_age()
    email = get_email()
    password = get_password()
    number = get_number()
    sex = get_sex()
    country = "Россия"
    city = get_random_city()
    is_physycal = get_bool()
    is_married = get_bool()
    amount_of_pur = get_amount_of_purchases()
    number_of_pur = get_number_of_purchase()
    rating = amount_of_pur/number_of_pur
    cluster=0
    user = (name, last_name, age, email, password, number, sex, country, city, is_physycal, is_married, amount_of_pur, number_of_pur, rating, cluster)

    return user