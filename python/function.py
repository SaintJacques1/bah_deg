from sklearn import preprocessing
import numpy as np

def select_all_and_print(cursor):
    try:
        cursor.execute('SELECT * FROM users LIMIT 10')
        records = cursor.fetchall()

        for row in records:
            print(row)
    except:
        print("Can't SELECT all from users table")


def insert_values_into_users(cursor, conn, user):
    sql_query = """INSERT INTO 
        users(name, last_name, age, email, password, number, sex, country, city, is_physycal, is_married, amount_of_pur, number_of_pur, rating, cluster)
        VALUES
        (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

    try:
        cursor.execute(sql_query, user)
        conn.commit()

    except (Exception) as error:
        print("Can't INSERT to users table", error)

def select_all_and_return(cursor):
    try:
        cursor.execute('SELECT * FROM users LIMIT 10')
        records = cursor.fetchall()

        return records
    except:
        print("Can't SELECT all from users table")

def update_rating(cursor, users):
    users_rating = []

    for i in users:
        users_rating.append(i[14])

    users_rating = np.array(users_rating).reshape(-1,1)
    minmax = preprocessing.MinMaxScaler(feature_range=(0,1))
    normalize_rating=minmax.fit_transform(users_rating)

    print("normalize", normalize_rating)

    users_list = []

    for i in users:
        users_list.append(list(i))

    for i in range(0, len(users)):
        users_list[i][14]=normalize_rating[i][0]

    users_tuple = []

    for i in users_list:
        users_tuple.append(tuple(i))

    return users_tuple

def update(cursor, conn, user_id, new_rating):
    sql = """UPDATE users SET name=%s WHERE id=%s"""

    try:
        cursor.execute(sql, ('dddd', new_rating))
        conn.commit()
        print("UPDAte success")
    except (Exception) as error:
        print("Can't UPDATE to users table", error)