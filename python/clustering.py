import psycopg2
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.cluster import KMeans

def update_cluster_number(cursor, conn, user_id, new_cluster_val):
    sql = """ UPDATE users
                SET cluster = %s
                WHERE id = %s"""

    try:
        cursor.execute(sql, (new_cluster_val, user_id))
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

try:
    conn = psycopg2.connect(
        dbname="prod",
        user="postgres",
        password="postgres",
        host="localhost"
    )

    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users')
    records = cursor.fetchall()
    print(len(records))

    # for row in records:
    #     print(row)
    users = records
    id = []
    age = []
    sex = []
    country = []
    city = []
    is_physycal = []
    is_married = []
    amount = []
    number = []
    rating = []
    cluster = []

    for i in records:
        id.append(i[0])
        age.append(i[3])
        sex.append(i[7])
        country.append(i[8])
        city.append(i[9])
        is_physycal.append(i[10])
        is_married.append(i[11])
        amount.append(i[12])
        number.append(i[13])
        rating.append(i[14])
        cluster.append(i[15])

    encoder = LabelEncoder()

    df = pd.DataFrame({
        "age": age,
        "sex": encoder.fit_transform(sex),
        "country": encoder.fit_transform(country),
        "city": encoder.fit_transform(city),
        "is_physycal": encoder.fit_transform(is_physycal),
        "is_married": encoder.fit_transform(is_married),
        "amount": amount,
        "number": number,
        "rating": rating,
        "cluster": cluster
    })


    model = KMeans(n_clusters=5)
    model.fit(df)
    all_r = model.predict(df)

    print(all_r)

    df_clustering = pd.DataFrame({
        "id": id,
        "age": age,
        "sex": encoder.fit_transform(sex),
        "country": encoder.fit_transform(country),
        "city": encoder.fit_transform(city),
        "is_physycal": encoder.fit_transform(is_physycal),
        "is_married": encoder.fit_transform(is_married),
        "amount": amount,
        "number": number,
        "rating": rating,
        "cluster": all_r
    })

    print(df_clustering)

    for i in range(0, 39):
        print(df_clustering.values[i][10])
        id = i+1
        cluster_n = df_clustering.values[i][10]
        update_cluster_number(cursor, conn, id, cluster_n)


except (Exception, psycopg2.Error) as error:
    print("Error while connecting to postgresSQL", error)

finally:
    # Closing postgreSQL connection
    if(conn):
        cursor.close()
        conn.close()
        print("PostgreSQL connection is closed")

