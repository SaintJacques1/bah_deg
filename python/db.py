"""
    Connection to postgreSQL database and inserts N users
"""

import psycopg2

# import local  module
import function
import user_class

try:
    conn = psycopg2.connect(
        dbname = "prod",
        user="postgres",
        password="postgres",
        host="localhost")

    cursor = conn.cursor()

    cursor.execute("SELECT version()")
    record = cursor.fetchone()
    print("Connecting to postgreSQL database")

    for i in range(0, 10):
        user = user_class.create_user()
        function.insert_values_into_users(cursor, conn, user)



except (Exception, psycopg2.Error) as error:
    print("Error while connecting to postgresSQL", error)

finally:
    # Closing postgre SQL connection
    if(conn):
        cursor.close()
        conn.close()
        print("PostgreSQL connection is closed")
