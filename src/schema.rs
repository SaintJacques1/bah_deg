table! {
    current_adversting (id) {
        id -> Int4,
        target_cluster -> Int4,
        advertising_text -> Text,
        advertising_name -> Text,
    }
}

table! {
    scoring_results (id) {
        id -> Int4,
        userid -> Int4,
        cluster -> Int4,
        scoring_result -> Bool,
    }
}

table! {
    users (id) {
        id -> Int4,
        name -> Varchar,
        last_name -> Varchar,
        age -> Int4,
        email -> Varchar,
        password -> Varchar,
        number -> Varchar,
        sex -> Varchar,
        country -> Varchar,
        city -> Varchar,
        is_physycal -> Bool,
        is_married -> Bool,
        amount_of_pur -> Float4,
        number_of_pur -> Int4,
        rating -> Float4,
        cluster -> Int4,
    }
}

joinable!(scoring_results -> users (userid));

allow_tables_to_appear_in_same_query!(
    current_adversting,
    scoring_results,
    users,
);
