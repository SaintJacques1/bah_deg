use super::schema::{scoring_results, current_adversting};

extern crate serde;
use serde::{Serialize, Deserialize};

#[derive(Queryable, Debug, Serialize)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub last_name: String,
    pub age: i32,
    pub email: String,
    pub password: String,
    pub number: String,
    pub sex: String,
    pub country: String,
    pub city: String,
    pub is_physycal: bool,
    pub is_married: bool,
    pub amount_of_pur: f32,
    pub number_of_pur: i32,
    pub rating: f32,
    pub cluster: i32,
}

#[derive(Queryable, Debug)]
pub struct ScoringTable {
    pub id: i32,
    pub userid: i32,
    pub cluster: i32,
    pub scoring_result: bool,
}

#[derive(Insertable)]
#[table_name = "scoring_results"]
pub struct NewUserWithScoringCoeff {
    pub userid: i32,
    pub cluster: i32,
    pub scoring_result: bool,
}

/*
    Стуктура, содержащая параметры весовых коэффициентов модели
*/
#[derive(Debug, Serialize, Deserialize)]
pub struct ScoringCoeff {
    pub country: i32,
    pub city: (i32, i32),
    pub sex: (i32, i32),
    pub age: (i32, i32, i32),
    pub is_mar: (i32, i32),
    pub is_phys: (i32, i32),
    pub rating: (i32, i32, i32),
    pub threshold: i32,

#[derive(Queryable, Debug, Serialize)]
pub struct CurrentAdversting {
    pub id: i32,
    pub target_cluster: i32,
    pub advertising_text: String,
    pub advertising_name: String,
}

#[derive(Insertable)]
#[table_name = "current_adversting"]
pub struct NewCurrentAdversting {
    pub target_cluster: i32,
    pub advertising_text: String,
    pub advertising_name: String,
}
