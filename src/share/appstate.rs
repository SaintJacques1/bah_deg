use diesel::prelude::PgConnection;


pub struct AppState {
    pub db_connection: PgConnection,
}