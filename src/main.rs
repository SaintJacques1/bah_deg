mod app;
mod api;
mod models;
mod schema;
mod share;
mod database;
mod lib;

#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate serde;

use actix_web::{server, App, HttpResponse, HttpRequest, http::Method, middleware::cors::Cors, middleware::cors::CorsBuilder};
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;

use app::init_app;
use share::{appstate::AppState};

use schema::users::dsl::*;
use models::{NewUserWithScoringCoeff, ScoringCoeff, ScoringTable, User};

fn main() {

    server::new(|| {
        init_app()
    })
        .bind("127.0.0.1:8088")
        .unwrap()
        .run();
}


