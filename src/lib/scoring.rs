use diesel::prelude::*;
use diesel::pg::PgConnection;
use actix_web::Json;

use super::super::models::{User, ScoringCoeff, NewUserWithScoringCoeff, NewCurrentAdversting};
use super::super::database::connection::establish_connection;

pub fn scoring(model_coeff: ScoringCoeff) {
    use super::super::schema::users::dsl::*;
    use super::super::schema::current_adversting;


    let connection = establish_connection();
    let res = users.limit(10000).load::<User>(&connection).expect("Err");
    println!("Scoring fn");
    for i in res {
        scoring_item(&connection, i, &model_coeff)
    }

    // определение целевого кластера
    let clust_num = compute_targeting_cluster();

    let new = NewCurrentAdversting {
        target_cluster: clust_num,
        advertising_text: "Новый автомобиль всего от 300000 рублей".to_string(),
        advertising_name: "Реклама автомобиля".to_string(),
    };

    diesel::insert_into(current_adversting::table)
        .values(new)
        .execute(&connection);
}


pub fn scoring_item(conn: &PgConnection, user: User, model_coeff: &ScoringCoeff) {

    println!("Scoring item ");
    let mut cnt = 0;
    let mut c = 0;
    let mut s = 0;
    let mut a = 0;
    let mut ism = 0;
    let mut isph = 0;
    let mut r = 0;

    if user.country == "Россия" {
        cnt = model_coeff.country;
    } else {
        cnt = 0;
    }

    if user.city == "Москва" {
        c = model_coeff.city.0;
    } else {
        c = model_coeff.city.1;
    }

    if user.sex == "Мужчина" {
        s = model_coeff.sex.0;
    } else {
        s = model_coeff.sex.1;
    }

    if user.age < 30 {
        a = model_coeff.age.0;
    } else if user.age > 30 && user.age < 50 {
        a = model_coeff.age.1;
    } else {
        a = model_coeff.age.2;
    }

    if user.is_married == true {
        ism = model_coeff.is_mar.0;
    } else if user.is_married == false {
        ism = model_coeff.is_mar.1;
    }

    if user.is_physycal == true {
        isph = model_coeff.is_phys.0;
    } else if user.is_physycal == false {
        isph = model_coeff.is_phys.1;
    }

    if user.rating <= 0.0003 {
        r = model_coeff.rating.0;
    } else if user.rating > 0.0003 && user.rating <= 0.0007 {
        r = model_coeff.rating.1;
    }

    let scoring_coeff = cnt + c + s + a + ism + isph + r;

    let scoring_user = NewUserWithScoringCoeff {
        userid: user.id,
        cluster: user.cluster,
        scoring_result: scoring_coeff >= model_coeff.threshold,
    };

    create_scoring_user(conn, scoring_user)
}

pub fn create_scoring_user(conn: &PgConnection, user: NewUserWithScoringCoeff) {
    use super::super::schema::scoring_results;
    diesel::insert_into(scoring_results::table)
        .values(user)
        .execute(conn);
}

pub fn compute_targeting_cluster() -> i32 {
    use super::super::schema::users::dsl::*;

    let mut cluster1 = 0;
    let mut cluster2 = 0;
    let mut cluster3 = 0;
    let mut cluster4 = 0;
    let mut cluster5 = 0;

    let connection = establish_connection();
    let res = users.limit(10000).load::<User>(&connection).expect("Err");

    for i in res {
        match i.cluster {
            0 => cluster1 += 1,
            1 => cluster2 += 1,
            2 => cluster3 += 1,
            3 => cluster4 += 1,
            4 => cluster5 += 1,
            _ => println!("another value"),
        }
    }

    println!("cluster number: {}, {}, {}, {}, {}", cluster1, cluster2, cluster3, cluster4, cluster5);

    let mut res = vec!(cluster1, cluster2, cluster3, cluster4, cluster5);
    let mut max_value = res.iter().fold(0i32, |mut max, &val| {
        if val > max {
            max = val
        }

        max
    });

    let mut index = 0;

    for i  in (0..res.len()) {
        if res[i] == max_value {
            index = i;
        }
    }
    println!("cluster num: {}", index);
    println!("max number: {}", max_value);

    index as i32
}
