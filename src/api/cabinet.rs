use actix_web::{http::Method, HttpRequest, HttpResponse, Path, Json};
use std::process::Command;

use crate::diesel::query_dsl::limit_dsl::LimitDsl;
use crate::diesel::RunQueryDsl;
use serde_json;

use super::super::lib::scoring::scoring;
use super::super::models::ScoringCoeff;
use super::super::{database::connection::establish_connection, models::User};
use super::super::schema::users::dsl::*;

pub fn index(req: &HttpRequest) -> HttpResponse {
    use super::super::schema::users::dsl::*;
    let connection = establish_connection();
    let res = users.limit(100).load::<User>(&connection).expect("Err");
    HttpResponse::Ok().json(res)
}

pub fn create(model: Json<ScoringCoeff>) -> HttpResponse {
    let model_s = ScoringCoeff {
        country: model.country,
        city: (model.city.0, model.city.1),
        sex: (model.sex.0, model.sex.1),
        age: (model.age.0, model.age.1, model.age.2),
        is_mar: (model.is_mar.0, model.is_mar.1),
        is_phys: (model.is_phys.0, model.is_phys.1),
        rating: (model.rating.0, model.rating.1, model.rating.2),
        threshold: model.threshold,
    };

    scoring(model_s);
    HttpResponse::Ok().body("ok")
}

pub fn clustering(req: &HttpRequest) -> HttpResponse{
    Command::new("python3")
        .arg("python/clustering.py")
        .spawn()
        .expect("Failed to run command");

    HttpResponse::Ok().body("ok")
}