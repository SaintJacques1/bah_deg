use actix_web::{App, http::Method, middleware::cors::Cors, middleware::cors::CorsBuilder};
use super::share::appstate::AppState;
use super::api::cabinet::{index, create, clustering};

pub fn init_app() -> App {
    App::new()
        .prefix("/app")
        .configure(|app| {
            Cors::for_app(app)
                .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
                .resource("/get_all", |r| r.method(Method::GET).f(index))
                .resource("/create", |r| r.method(Method::POST).with(create))
                .resource("/clustering", |r| r.method(Method::GET).f(clustering))
                .register()
        })
}
